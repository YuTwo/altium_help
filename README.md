## Подсказки по Altium

### Общее:  
- DXP\Preference - Общие настройки здесь.  
- Alt+F5 - Вид на полный экран.  
- Ctrl-M - Измерение, Shift-C чтобы стереть измерения.  
- Shift-F - Поиск подобных.  
- Shift-Ctrl-D - Привязать к сетке.  
- T-L - Обновление из библиотеки (Menu\Tools\Update From Libraries).  
- V-F - Масштаб на всю плату или схему.  
- Относительные пути в папке проекта начинаются с точки, например .\Backup\ для автосохранения, настраивается в DXP\Preferences\Data Menegment\Backup\Path.  

### Создание компонентов:

#### Слои:
- Mechanical 1   - Top Assy - Контуры компонентов
- Mechanical 2   - Bottom Assy - Контуры компонентов
- Mechanical 4   - Inside - Вырезы в плате
- Mechanical 5   - Size - Размеры и прочее
- Mechanical 13  - 3D model
- Mechanical 15  - Border (Граница компонента при размещении)

Создание экструдированного 3D: Menu/Tools/Manage 3D Body for Current Component...

Чтобы отображались Designator и Coment нужно поставить галочку в свойствах документа в библиотеке УГО: 
ПКМ\Options\Document Options\Library Editor Options\Always Show Comments/Designator


### Схема:
- V-U - переключение единиц измерения мм/дюймы
- X - отображение относительно оси X
- Y - отображение относительно оси Y
- T-A-A - Перемаркировка позиционных обозначений
- P-W - Соединение
- Подсветка цепей в SCH - нажать на иконку с карандашиком и выбрать цепь

- Символ надчеркивания в цепях: либо ПОСЛЕ каждой буквы вставить \ ,  
  либо ПЕРЕД всем текстом вставить \ и включить DXP/Preferences/Schematic/Grafical Editing/Single`\`Negation  
- Настройка примитивов по умолчанию - DXP/Preferences/Schematic/Default Primitives

Выделение на плате элементов, выделенных на схеме – после выделения элементов на принципиальной схеме, Правая Кнопка Мышки – Part Action – Select PCB Components.

### Плата:
После создания платы создаем слои по образцу (L).  
Слои Mechanical 1 и Mechanical 2 объединить в пару.  

Чтобы заменялись специальные строки на PCB (.Designator итп) - L/View Options/Display Options/Convert Special String  
Вспомогательные линии или Work Guides. Как их убрать? Tools/Guide Manager. Снять галку, чтобы скрыть, либо ПКМышки - Delete All.  
Редактирование размера платы - в режиме Board Planing Mode (1) - Menu/Design/Edit Board Shape  

- 1, 2, 3 виды PCB (Определение границ платы, 2d, 3d)  
- Q - Переключение единиц измерения мм/дюймы  
- G - Выбор сетки из списка. Для редактирования пункта нажать на него с Ctrl.  
- L - Переместить на другую сторону (Удерживать нажатой ЛКМ на объекте).  
- Пробел - Поворот (Удерживать нажатой ЛКМ на объекте).  
- Ctrl-H - Выделение всей цепи  
- Shift-H - Отключение-включение Insight окна  
- Shift-G - Открепляет и прикрепляет окошко Insight.  
- Shift-S - Режим одного слоя  
- Ctrl-Click - Выделение всей цепи  
- S-S - Выделить всю дорожку на одном слое (также если выделить кусок и нажать TAB)  
- P-T - Добавить дорожку (3 - смена ширины дорожки из правил мин/рек/макс)  
- Shift-W - Меню выбора ширины дорожки (при разводке)  
- Shift-Пробел - Изменение способа проведения трассы  
- E-O-S - Установка начала координат платы (Menu/Edit/Origin/Set)  
- V-G-G - Установка специфической сетки  
- D-R - Правила  
- M-D - Перемещать компонент вместе с дорожками  
- P-L - Рисовать линию  
- T-M - Стереть маркеры ошибок (Menu/Tools/Reset Error Markers)  
- V-B - Вид с обратной стороны (зеркально)  
- \* или (Ctrl-Shift-колесико) - Переходное отверстие на другой слой  
- \+ или \- - Переключение между слоями при проведении трассы  

Перенести на другой слой - Выделить объект затем ПКМ/Properties... там выбрать слой куда перенести и OK.  
Задать любимую ширину дорожек - Menu/Tools/Preferences.../PCB Editor/Interactive Routing/Favorite Interactive Routing Widths  
Задать предлагаемую ширину дорожки - Menu/Tools/Preferences.../PCB Editor/Interactive Routing/Interactive Routing Width Source/Track Width Mode/Rule Preferred  

### Полигоны:
- P-G - Установка параметров заливки. По окончании - заливка.  
- Перезаливка - выделить полигон, затем ПКМ Poligon Action/Repour Selected.  

### Вывод:
- При печати через Output Job Files в PDF чтобы размер печати совпадал с реальным нужно указать в свойствах PDF в Output Containers/Advanced/PCB Page Size and Orientation Source: Page Setup Dialog  

- для ЛУТ: File — Fabrication Outputs — ODB++ Files. Затем Tables — Layers. 
Там снимаем галки со всех слоев, кроме Top или Bottom, в зависимости от нужного слоя — верхний или нижний, 
ставим галки на Drill и Keepout или Mechanical4, в зависимости где нарисован контур платы, если есть.  
Затем меняем цвета — Top или Bottom ставим черный, Drill — белый, Keepout или Mechanical4 — черный.  
Затем File — Print Preview — Print Color = Color, User Scale = 1,0.   

### Для создания панелей:
Добавьте в проект пустую плату. Установите метрические единицы измерения (Q). Установите размеры.
Сохраните с именем например Pаnel_PCB.PcbDoc. Далее Place/Embedded Board Array/Panelize выберите в Property/PCB Document существующую плату.
Настройте количество плат в рядах и колонках, а также зазоры между ними.
Добавьте в Output Containers - PDF. Укажите в свойствах PDF в Output Containers/Advanced/PCB Page Size and Orientation Source: Page Setup Dialog.
Добавьте в проект Output Job Files. Добавьте в раздел Documentation Outputs плату с панелью, созданную на предыдущем шаге. На ней ПКМышки Configure...
Выберите нужные слои (удалять ПКМышки на слое и Delete), затем там же кнопка Prefereces... настройте необходимые цвета (что чёрным, а что белым).
ПКМышки на файле Page Setup... Scaling/Scaled Print Scale 1.00. Color Set - Mono. Установите размер бумаги и разрешение. Затем кнопка Preview.
Если всё нормально то печатаем.

### Altium Designer and pads with drill holes on photomask.
Some people use to make PCB at home by method of Laser-Iron-Textolite or the photoresist method. 
They route an PCB and then print it on paper in b/w. Then PCB is tinned and drilled. 
It is very easy to drill an PCB if copper pads which are to be drilled already have a hall in the middle. 
In this way drill is so to say auto-aimed by the borders of solder on copper pad and does not slide out.
But it is not so easy to get photomask with hall in pads by means of AD. But it is possible.
True way: create two gerber files with bottom and top layers, and drill file. 
These actions are accessed in PCB mode through File-Fabrication options-[Gerber files, NC Drill Files] . 
So, we have 3 files. Then, switch to "top" gerber. Go to File-Import-Quick load. 
Then in the dialog select the path "project_name_Files" and there will be one .txt file "project_name.txt", choose it. 
Then go to View-Workspace Panels-CamStatic-CamStatic. In CamStatic dialog switch off top layer and drill. 
Go to Edit-Composite layers-Build Composite. You must set name of new layer lest AD crashes. 
First layer in list will be the top layer, and the second will be the drill. 
In Polarity column you select Dark for wire layer and Clear for drill, and go further. That's all.
For LIT you mirror resulting layer if it is top when for photoresist method you mirror the bottom. 
Mirroring is accessed via Tools->Film wizard.
PS. If you see the haircrosses on drill holes, go to View and select Cam Editor instead of NC Editor.

### Классы:
Если для каких-то цепей или компонентов на схеме требуется выполнение особых правил разводки (например, определенная толщина дорожек, или особый способ подключения выводов компонента к полигонам), передачу информации в ПП можно выполнить с помощью Классов Компонентов (или Классов Цепей). Впоследствии, при проектировании ПП, для этих классов можно написать правило разводки.
Установка класса компонента (Component Class) выполняется путем добавления пользовательского параметра (User Parameter) с именем «ClassName». При этом значением этого параметра должно стать имя класса, которое мы хотим сопоставить компоненту. Например, при разводке платы XX222, для компонентов, которые требовали подключения к полигонам без тепловых барьеров, был прописан класс «Direct_connect», для которого впоследствии было создано специальное правило разводки.
Установка класса цепи (NetClass) выполняется путем присоединения к соответствующей цепи на принципиальной схеме директивы «Net Class»: Place – Directives – Net Class. После выполнения этой команды появляется специальный значок, который необходимо присоединить к искомой цепи. Потом двойным щелчком вызываем окно свойств этой директивы (это можно сделать еще перед присоединением к цепи, нажав в процессе выполнения команды клавишу TAB). В окне свойств необходимо заполнить два поля: поле «Name», в котором указывается название директивы так, как оно будет выглядеть на принципиальной схеме; и поле Value, в котором необходимо указать название класса цепи. Именно это название класса и будет впоследствии передано в печатную плату.
 Организовать новый класс или отредактировать существующий можно и на печатной плате. Для этого необходимо воспользоваться меню Design – Classes.

### Room:
Альтиум позволяет объединять компоненты в группы, сопоставляя каждой группе свою область размещения – Room. 
Определить Room можно как из схемы, так и прямо на печатной плате.
- Определение из принципиальной схемы. По умолчанию, Альтиум предлагает с каждого листа принципиальной схемы создавать свою область размещения. 
Кроме того, возможно создание областей размещения для компонентов, имеющих одинаковый класс (Component Class). 
Режимы создания областей размещения определяются положением галочек в Project Options, на закладке Class Generation. 
Окно «Project Options» можно вызвать из меню «Project» (при работе в схемном редакторе).
- Определение в редакторе печатных плат. Выделяем необходимые компоненты, 
затем Design/Room/Create <несколько вариантов> Room from selected component – создать область размещения (есть разные варианты – см.названия пунктов в меню).
Созданная область размещения передвигается по плате вместе с составляющими ее компонентами. 
При вынесении какого-нибудь из компонентов за пределы области, и область и компонент подсвечиваются зеленым цветом, что сигнализирует об ошибке. 
Это верно для того случая, когда созданная область размещения имеет свойство «Keep component Inside» - держать элементы внутри. 
С точностью до наоборот работает свойство «Keep component Outside».

### Импорт из PCAD:
- File/Import Wizard  

Горячие клавиши
http://microsin.net/adminstuff/others/altium-designer-editor-shortcuts.html

FAQ
http://cxem.net/comp/comp150.php
http://forum.cxem.net/index.php?/topic/95427-faq-%D0%BF%D0%BE-%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B0%D0%BC%D0%BC%D0%B5-altium-designer/&tab=comments#comment-1049059

Фишки
https://adelectronics.ru/2019/04/18/altium-designer-19-%D1%81%D0%BE%D0%B2%D0%B5%D1%82%D1%8B-%D0%B8-%D1%84%D0%B8%D1%88%D0%BA%D0%B8-%D1%83%D0%BF%D1%80%D0%BE%D1%89%D0%B0%D1%8E%D1%89%D0%B8%D0%B5-%D0%BF%D1%80%D0%BE%D0%B5%D0%BA%D1%82%D0%B8/

Книги
https://electronix.ru/forum/index.php?app=forums&module=forums&controller=topic&id=97354

3d модели
http://www.3dcontentcentral.com/Download-Model.aspx?catalogid=171&id=817657

-EOF-